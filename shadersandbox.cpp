// Shader sandbox  v1.8
//     by HolyBlackCat (std-cin@mail.ru), Mar 27 2015
//
// Generates images using GLSL shaders.
// Requires SDL 2.0.3 (or later) and GLEW
//
// Latest binary here: https://bitbucket.org/HolyBlackCat/ld48/downloads/ShaderSandbox.exe
// Source code:        https://bitbucket.org/HolyBlackCat/ld48/src
//
// Changelog:
//   1.8
//     Fixed image stretching when mouse is near window's border and image is zoomed
//     More zoom modes: 1x,2x,3x,4x,5x,6x,7x,8x instead of 1x,2x,4x,8x
//
// No special compilation flags needed, just -O3 -s -std=c++1x


#define GLEW_STATIC              /// Comment this line if you want dynamic linking for GLEW.
#define MACOS_CONTEXT_SETTINGS 0 /// 0 - compatibiity profile, 1 - core profile and forward compatible context


#include <cmath>
#include <csignal>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <list>
#include <fstream>
#include <initializer_list>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <utility>
#include "lib/GL/glew.h"
#include "lib/SDL2/SDL.h"

#define EXEC_ONCE \
do {static bool EXEC_ONCE_FLAG = 0; if (EXEC_ONCE_FLAG) Exit(Str({"Error: ", __func__, "() called twice."})); EXEC_ONCE_FLAG = 1;} while (0)

[[noreturn]] void Exit()
{
	SDL_Quit();
	std::exit(0);
}

[[noreturn]] void Exit(const char *txt)
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "ShaderSandbox - Error", txt, 0);
	SDL_Quit();
	std::exit(0);
}

inline namespace StrConcatFunc
{
	class UC
	{
	public:
		static std::stringstream ss;
		std::string str;
		template <class T> UC(T param) : str((ss.str(""), ss << param, ss.str())) {}
	};

	std::stringstream UC::ss;

	const char *Str(std::initializer_list<UC> data)
	{
		static std::string ret;
		ret = "";
		for (auto it = data.begin(); it != data.end(); it++)
		{
			ret += it->str;
		}
		return ret.c_str();
	}
}

void Init()
{
	EXEC_ONCE;
	auto SignalHandler = [](int sig) -> void
	{
		switch (sig)
		{
		case SIGINT: case SIGTERM:
		#ifdef SIGBREAK
		case SIGBREAK:
		#endif
			Exit();
			break;

		case SIGILL:
			Exit("SIGILL: Illegal instruction.");
			break;

		case SIGFPE:
			Exit("SIGFPE: Floating point error.");
			break;

		case SIGSEGV:
			Exit("SIGSEGV: Segmentation violation.");
			break;

		case SIGABRT:
			Exit("SIGABRT: Abnormal termination.");
			break;

		default:
			Exit("Unknown signal.");
			break;
		}
	};

	std::signal(SIGINT  , SignalHandler);
	std::signal(SIGTERM , SignalHandler);
	std::signal(SIGILL  , SignalHandler);
	std::signal(SIGFPE  , SignalHandler);
	std::signal(SIGSEGV , SignalHandler);
	std::signal(SIGABRT , SignalHandler);
	#ifdef SIGBREAK
	std::signal(SIGBREAK, SignalHandler);
	#endif

	std::set_terminate(
	[]{
		try {throw;}
		catch (const std::exception &e) {Exit(Str({"Standard exception: `", e.what(), "`."}));}
		catch (const char *p) {Exit(Str({"Exception: `", p, "`."}));}
		catch (...) {Exit(Str({"Unknown exception."}));}
	});
	std::set_unexpected(
	[]{
		try {throw;}
		catch (const std::exception &e) {Exit(Str({"Unexpected standard exception: `", e.what(), "`."}));}
		catch (const char *p) {Exit(Str({"Unexpected exception: `", p, "`."}));}
		catch (...) {Exit(Str({"Unexpected unknown exception."}));}
	});

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS))
		Exit(Str({"SDL init failed: `", SDL_GetError(), "`."}));

	glewExperimental = GL_TRUE;
}

struct StringPair {const char *s1, *s2;};

// first - uniform name; second - file name; ?_wrap: 0 - clamp, 1 - mirror, 2 - repeat, 3 - transparent
struct TextureData {std::string first, second; bool filt_linear; int h_wrap, v_wrap; bool exist;};

const char *const baddatastr = "Invalid data or format, please try again.\n";

int AskList(const char *txt, std::initializer_list<StringPair> list)
{
	std::string str;

  repeat:
  	std::cout << txt << '\n';
	for (auto &it : list)
	{
		std::cout << "   " << it.s1 << " - " << it.s2 << '\n';
	}
	std::getline(std::cin, str);
	for (auto &it : str) if (it >= 'A' && it <= 'Z') it += 'a' - 'A';
	int pos = 0;
	for (auto &it : list)
	{
		if (!std::strcmp(str.c_str(), it.s1))
			return pos;
		pos++;
	}
	std::cout << baddatastr;
	goto repeat;

}

int winsize_w = 800, winsize_h = 600, zoom_level = 1, win_maximize = 0;
int winpos_x = SDL_WINDOWPOS_UNDEFINED, winpos_y = SDL_WINDOWPOS_UNDEFINED;
bool savedims;
int img_w, img_h;
std::list<TextureData> uni_tex_list;

GLuint fbuffer, rbuffer;
std::string prjfilename, prjimagename, inp_str;
std::stringstream ss;

SDL_Window *winhandle;
SDL_GLContext conhandle;

void SaveImage()
{
	std::ofstream out_f(prjimagename.c_str(), std::ios_base::out | std::ios_base::binary);
	if (!out_f)
	{
		std::cout << "Can't save image to `" << prjimagename << "`.\n";
		return;
	}
	out_f << (unsigned char)(0x00); // 00
	out_f << (unsigned char)(0x00); // 01
	out_f << (unsigned char)(0x02); // 02
	out_f << (unsigned char)(0x00); // 03
	out_f << (unsigned char)(0x00); // 04
	out_f << (unsigned char)(0x00); // 05
	out_f << (unsigned char)(0x00); // 06
	out_f << (unsigned char)(0x00); // 07
	out_f << (unsigned char)(0x00); // 08
	out_f << (unsigned char)(0x00); // 09
	out_f << (unsigned char)(0x00); // 0A
	out_f << (unsigned char)(0x00); // 0B
	out_f << (unsigned char)(img_w % 256); // 0C
	out_f << (unsigned char)(img_w / 256); // 0D
	out_f << (unsigned char)(img_h % 256); // 0E
	out_f << (unsigned char)(img_h / 256); // 0F
	out_f << (unsigned char)(0x20); // 10
	out_f << (unsigned char)(0x08); // 11

	char *arr = new char[img_w * img_h * 4];
	glReadPixels(0, 0, img_w, img_h, GL_BGRA, GL_UNSIGNED_BYTE, arr);
	out_f.write(arr, img_w * img_h * 4);

	delete [] arr;
	std::cout << "Image saved.\n";
}

void LoadStubToBoundTexture()
{
	static constexpr unsigned char stub[4*4*4] {0x00, 0x00, 0x7f, 0xff,  0x00, 0x00, 0xff, 0xff,  0x00, 0xff, 0x00, 0xff,  0x00, 0x7f, 0x00, 0xff,
												0x00, 0x00, 0xff, 0xff,  0x00, 0x00, 0xff, 0xff,  0x00, 0xff, 0x00, 0xff,  0x00, 0xff, 0x00, 0xff,
												0xff, 0x00, 0x00, 0xff,  0xff, 0x00, 0x00, 0xff,  0xff, 0xff, 0xff, 0xff,  0xff, 0xff, 0xff, 0xff,
												0x7f, 0x00, 0x00, 0xff,  0xff, 0x00, 0x00, 0xff,  0xff, 0xff, 0xff, 0xff,  0x7f, 0x7f, 0x7f, 0xff};
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 4, 4, 0, GL_BGRA, GL_UNSIGNED_BYTE, stub);
}

void LoadTextures()
{
	static constexpr const char *err1 = "WARNING: Can't load `",
								*err2 = "`: ",
								*err3 = ". Stub is used.\n";

	int tex_counter = GL_TEXTURE0;
	for (const auto &it : uni_tex_list)
	{
		if (!it.exist)
			continue;
		glActiveTexture(tex_counter++);

		std::ifstream inp_file(it.second, std::ios_base::in | std::ios_base::binary);
		if (!inp_file)
		{
			std::cout << "WARNING: Can't access file `" << it.second << "`. Stub is used.\n";
			LoadStubToBoundTexture();
			continue;
		}
		inp_file >> std::noskipws;
		unsigned char idfieldlen, tmp;
		int img_w, img_h;
		bool swap_x, swap_y;

		inp_file >> idfieldlen;																							// 00
		inp_file >> tmp; if (tmp != 0x00) {std::cout << err1 << it.second << err2 << "Files with colormaps are not supported" << err3; LoadStubToBoundTexture(); continue;} // 01
		inp_file >> tmp; if (tmp != 0x02) {std::cout << err1 << it.second << err2 << "File must uncompressed 32 bpp TrueColor image (type check failure)" << err3; LoadStubToBoundTexture(); continue;} // 02
		inp_file >> tmp; if (tmp != 0x00) {std::cout << err1 << it.second << err2 << "Files with colormaps are not supported (1)" << err3; LoadStubToBoundTexture(); continue;} // 03
		inp_file >> tmp; if (tmp != 0x00) {std::cout << err1 << it.second << err2 << "Files with colormaps are not supported (2)" << err3; LoadStubToBoundTexture(); continue;} // 04
		inp_file >> tmp; if (tmp != 0x00) {std::cout << err1 << it.second << err2 << "Files with colormaps are not supported (3)" << err3; LoadStubToBoundTexture(); continue;} // 05
		inp_file >> tmp; if (tmp != 0x00) {std::cout << err1 << it.second << err2 << "Files with colormaps are not supported (4)" << err3; LoadStubToBoundTexture(); continue;} // 06
		inp_file >> tmp; if (tmp != 0x00) {std::cout << err1 << it.second << err2 << "Files with colormaps are not supported (5)" << err3; LoadStubToBoundTexture(); continue;} // 07
		inp_file.ignore(4);                                                                                             // 08
																														// 09
																														// 0a
																														// 0b
		inp_file >> tmp; img_w = tmp;																					// 0c
		inp_file >> tmp; img_w |= tmp << 8;																				// 0d
		inp_file >> tmp; img_h = tmp;																					// 0e
		inp_file >> tmp; img_h |= tmp << 8;																				// 0f
		inp_file >> tmp; if (tmp != 0x20) {std::cout << err1 << it.second << err2 << "File must uncompressed 32 bpp TrueColor image (bpp check failure)" << err3; LoadStubToBoundTexture(); continue;} // 10
		inp_file >> tmp; if ((tmp&15)!=8) {std::cout << err1 << it.second << err2 << "File must uncompressed 32 bpp TrueColor image (alpha check failure)" << err3; LoadStubToBoundTexture(); continue;} /* 11 */ swap_x = tmp & 0x10; swap_y = !(tmp & 0x20);

		inp_file.ignore(idfieldlen);

		if (!inp_file)                    {std::cout << err1 << it.second << err2 << "Unexpected end of the file (header)" << err3; LoadStubToBoundTexture(); continue;}

		char (*data)[4] = new char[img_w * img_h][4];
		int x, y, xend, yend, xstep, ystep, xbegin;

		if (!swap_x)
		{
			xbegin = 0;
			xend = img_w;
			xstep = 1;
		}
		else
		{
			xbegin = img_w - 1;
			xend = -1;
			xstep = -1;
		}
		if (!swap_y)
		{
			y = 0;
			yend = img_h;
			ystep = 1;
		}
		else
		{
			y = img_h - 1;
			yend = -1;
			ystep = -1;
		}


		while (y != yend)
		{
			x = xbegin;
			while (x != xend)
			{
				auto &ref = data[x + img_w * y];
				inp_file >> ref[0];
				inp_file >> ref[1];
				inp_file >> ref[2];
				inp_file >> ref[3];
				x += xstep;
			}
			y += ystep;
		}//*/
		/*
		for (int y = 0; y < img_h; y++)
		{
			for (int x = 0; x < img_h; x++)
			{
				auto &ref = data[x + img_w * y];
				inp_file >> ref[0];
				inp_file >> ref[1];
				inp_file >> ref[2];
				inp_file >> ref[3];
			}
		}//*/
		if (!inp_file) {std::cout << err1 << it.second << err2 << "Unexpected end of the file" << err3; LoadStubToBoundTexture(); continue;}

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, img_w, img_h, 0, GL_BGRA, GL_UNSIGNED_BYTE, data);
		delete [] data;
	}
}

bool OpenWindow(bool need_preview) // returns 0 on fail
{
	img_w = img_h = 0;
	static const char *const parse_fail_str_header = "Project file header parsing failed.\n",
					  *const parse_fail_str_data   = "Project file header contains invalid data.\n",
					  *const parse_fail_str_delim  = "Project file does not contain /*~~~~~~~*/ delimiter.\n";

	std::ifstream inp_file(prjfilename.c_str(), std::ios_base::in | std::ios_base::binary);
	if (!inp_file)
	{
		std::cout << '`' << prjfilename << "` not found.\n";
		return 0;
	}
	inp_file >> std::noskipws;
	char tmp;
	bool flag;
	int v_major = 0, v_minor = 0;
	inp_file >> tmp; if (tmp != '/') {std::cout << parse_fail_str_header; return 0;}
	inp_file >> tmp; if (tmp != '*') {std::cout << parse_fail_str_header; return 0;}
	while (1)
	{
		inp_file >> tmp;
		switch (tmp)
		{
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			v_major = v_major * 10 + tmp - '0';
			break;
		  case '.':
			goto loopbreak1;
			break;
		  default:
			std::cout << parse_fail_str_header; return 0;
			break;
		}

		goto skipbreak1;
	  loopbreak1:
	  	break;
	  skipbreak1:;
	}
	if (v_major < 3) {std::cout << parse_fail_str_data; return 0;}
	flag = 0;
	while (1)
	{
		inp_file >> tmp;
		switch (tmp)
		{
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			v_minor = v_minor * 10 + tmp - '0';
			break;
		  case '@':
			goto loopbreak2;
			break;
		  default:
			std::cout << parse_fail_str_header; return 0;
			break;
		}

		goto skipbreak2;
	  loopbreak2:
	  	break;
	  skipbreak2:
	  flag = 1;
	}
	if (!flag) {std::cout << parse_fail_str_data; return 0;}
	while (1)
	{
		inp_file >> tmp;
		switch (tmp)
		{
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			img_w = img_w * 10 + tmp - '0';
			break;
		  case 'x':
			goto loopbreak3;
			break;
		  default:
			std::cout << parse_fail_str_header; return 0;
			break;
		}

		goto skipbreak3;
	  loopbreak3:
	  	break;
	  skipbreak3:;
	}
	if (img_w <= 0) {std::cout << parse_fail_str_data; return 0;}
	while (1)
	{
		inp_file >> tmp;
		switch (tmp)
		{
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			img_h = img_h * 10 + tmp - '0';
			break;
		  case '*':
		  case '|':
			goto loopbreak4;
			break;
		  default:
			std::cout << parse_fail_str_header; return 0;
			break;
		}

		goto skipbreak4;
	  loopbreak4:
	  	break;
	  skipbreak4:;
	}
	if (img_h <= 0) {std::cout << parse_fail_str_data; return 0;}

	uni_tex_list.clear();
	while (tmp == '|')
	{
		uni_tex_list.push_back(TextureData());
		auto &ref = *--uni_tex_list.end();
		while (1)
		{
			inp_file >> tmp;
			if (tmp == '|')
				break;
			if (!((tmp >= 'A' && tmp <= 'z') || (tmp >= '0' && tmp <= '9') || tmp == '_'))
			{
				std::cout << parse_fail_str_data;
				return 0;
			}
			ref.first += tmp;
		}
		if ((ref.first[0] >= '0' && ref.first[0] <= '9') || ref.first[0] == '\0')
		{
			std::cout << parse_fail_str_data;
			return 0;
		}

		auto prevend = --uni_tex_list.end();
		for (auto it = uni_tex_list.begin(); it != prevend; it++)
		{
			if (it->first == ref.first)
			{
				std::cout << parse_fail_str_data;
				return 0;
			}
		}

		while (1)
		{
			inp_file >> tmp;
			if (tmp == '|')
				break;
			ref.second += tmp;
		}
		ref.second += ".tga";

		inp_file >> tmp;
		switch (tmp) {case 'N': break; case 'L': ref.filt_linear = 1; break; default: std::cout << parse_fail_str_data; return 0; break;}
		inp_file >> tmp;
		switch (tmp)
		{
		  case '_': break;
		  case 'M': ref.h_wrap = 1; break;
		  case 'R': ref.h_wrap = 2; break;
		  case 'T': ref.h_wrap = 3; break;
		  default: std::cout << parse_fail_str_data; return 0; break;
		}
		inp_file >> tmp;
		switch (tmp)
		{
		  case '_': break;
		  case 'M': ref.v_wrap = 1; break;
		  case 'R': ref.v_wrap = 2; break;
		  case 'T': ref.v_wrap = 3; break;
		  default: std::cout << parse_fail_str_data; return 0; break;
		}
		inp_file >> tmp;
	}

	if (tmp != '*') {std::cout << parse_fail_str_header; return 0;}
	inp_file >> tmp; if (tmp != '/') {std::cout << parse_fail_str_header; return 0;}

	static const char delim_str[] = "/*~~~~~~~*/";

	std::ifstream::pos_type shader1pos = inp_file.tellg(), shader2pos;
	int shader1len = -(sizeof delim_str / sizeof delim_str[0]) + 2, shader2len = 0, delim_pos = 0;
	while (1)
	{
		inp_file >> tmp;
		if (!inp_file) {std::cout << parse_fail_str_delim; return 0;}

		if (tmp == delim_str[delim_pos])
			delim_pos++;
		else
			delim_pos = 0;
		if (delim_pos == (sizeof delim_str / sizeof delim_str[0]) - 1)
			break;
		shader1len++;
	}
	shader2pos = inp_file.tellg();
	while (1)
	{
		inp_file >> tmp;
		if (!inp_file)
			break;
		shader2len++;
	}
	char *shader1src = new char[shader1len + 1],
		 *shader2src = new char[shader2len + 1];
	shader1src[shader1len] = shader2src[shader2len] = '\0';

	inp_file.clear();
	inp_file.seekg(shader1pos);
	for (int i = 0; i < shader1len; i++)
	{
		inp_file >> shader1src[i];
	}
	inp_file.seekg(shader2pos);
	for (int i = 0; i < shader2len; i++)
		inp_file >> shader2src[i];
	inp_file.close();


	SDL_SetHint(SDL_HINT_RENDER_VSYNC, "0");
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, v_major);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, v_minor);
	#if MACOS_CONTEXT_SETTINGS == 0
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
	#else
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	#endif

	savedims = need_preview;

	if (need_preview)
	{
		if (winsize_w < 256) winsize_w = 256;
		if (winsize_h < 256) winsize_h = 256;
		winhandle = SDL_CreateWindow("ShaderSandbox", winpos_x, winpos_y, winsize_w, winsize_h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | (win_maximize ? SDL_WINDOW_MAXIMIZED : 0));
		SDL_SetWindowMinimumSize(winhandle, 256, 256);
	}
	else
		winhandle = SDL_CreateWindow("ShaderSandbox - Hidden window", 100, 100, 256, 256, SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN);
	if (!winhandle)
	{
		std::cout << "Window creation failed.\n";
		return 0;
	}

	conhandle = SDL_GL_CreateContext(winhandle);
	if (!conhandle)
	{
		std::cout << "OpenGL context creation failed. (Probably specified version is not supported.)\n";
		SDL_DestroyWindow(winhandle);
		return 0;
	}

	SDL_GL_SetSwapInterval(0);

	if (glewInit())
	{
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		std::cout << "GLEW init failed.\n";
		return 0;
	}

	GLuint prog, vsh, fsh;

	prog = glCreateProgram();
	vsh = glCreateShader(GL_VERTEX_SHADER);
	fsh = glCreateShader(GL_FRAGMENT_SHADER);

	if (!prog)
	{
		std::cout << "glCreateProgram() failed.\n";
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}
	if (!(vsh && fsh))
	{
		std::cout << "glCreateShader() failed.\n";
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}

	glAttachShader(prog, vsh);
	glAttachShader(prog, fsh);
	glShaderSource(vsh, 1, &shader1src, 0);
	glShaderSource(fsh, 1, &shader2src, 0);
	delete [] shader1src;
	delete [] shader2src;

	glCompileShader(vsh);
	glCompileShader(fsh);

	GLint vstat, fstat;
	glGetShaderiv(vsh, GL_COMPILE_STATUS, &vstat);
	glGetShaderiv(fsh, GL_COMPILE_STATUS, &fstat);

	if (vstat != GL_TRUE || fstat != GL_TRUE)
	{
		char *vlog, *flog;
		glGetShaderiv(vsh, GL_INFO_LOG_LENGTH, &vstat);
		glGetShaderiv(fsh, GL_INFO_LOG_LENGTH, &fstat);
		if (vstat)
		{
			vlog = new char[vstat];
			glGetShaderInfoLog(vsh, vstat, 0, vlog);
		}
		else
			vlog = (char *) "<empty>";
		if (fstat)
		{
			flog = new char[fstat];
			glGetShaderInfoLog(fsh, fstat, 0, flog);
		}
		else
			flog = (char *) "<empty>";

		std::cout << Str({"Shader compilation failed:",
				         (vstat != GL_TRUE) ? "\nVertex shader: NOT OK":"\nVertex shader: OK",
				         (fstat != GL_TRUE) ? "\nFragment shader: NOT OK":"\nFragment shader: OK",
				         "\n  Vertex log:\n", vlog, "\n  Fragment log:\n", flog, '\n'});
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}
	glLinkProgram(prog);
	glGetProgramiv(prog, GL_LINK_STATUS, &vstat);
	if (vstat != GL_TRUE)
	{
		glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &vstat);
		char *log;
		if (vstat)
		{
			log = new char[vstat];
			glGetProgramInfoLog(prog, vstat, 0, log);
		}
		else
			log = (char *) "<empty>";

		std::cout << Str({"Shader linking failed.\n\n  Log:\n", log, '\n'});
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}
	glUseProgram(prog);


	GLuint vao;
	glGenVertexArrays(1, &vao);
	if (!vao)
	{
		std::cout << "Internal OpenGL error: vao creation failed.\n";
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}
	glBindVertexArray(vao);


	glGenRenderbuffers(1, &rbuffer);
	if (!rbuffer)
	{
		std::cout << "Internal OpenGL error: renderbuffer creation failed.\n";
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}
	glBindRenderbuffer(GL_RENDERBUFFER, rbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, img_w, img_h);

	glGenFramebuffers(1, &fbuffer);
	if (!fbuffer)
	{
		std::cout << "Internal OpenGL error: framebuffer creation failed.\n";
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, rbuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Internal OpenGL error: framebuffer is incomplete:\n";
		SDL_GL_DeleteContext(conhandle);
		SDL_DestroyWindow(winhandle);
		return 0;
	}


	int tex_counter = 0;
	for (auto &it : uni_tex_list)
	{
		GLint loc = glGetUniformLocation(prog, it.first.c_str());
		if (loc == -1)
		{
			std::cout << "WARNING: Texture " << it.first << '(' << it.second << ") is requested but not used.\n";
			tex_counter++;
			it.exist = 0;
			continue;
		}
		it.exist = 1;
		glUniform1i(loc, tex_counter);

		glActiveTexture(GL_TEXTURE0 + tex_counter);
		tex_counter++;
		GLuint tex;
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);

		static constexpr GLint table_wrap[] {GL_CLAMP_TO_EDGE, GL_MIRRORED_REPEAT, GL_REPEAT, GL_CLAMP_TO_BORDER};
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, it.filt_linear ? GL_LINEAR : GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, it.filt_linear ? GL_LINEAR : GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, table_wrap[it.h_wrap]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, table_wrap[it.v_wrap]);
	}

	LoadTextures();

	return 1;
}

void CloseWindow()
{
	if (savedims)
	{
		int xx, yy;
		SDL_GetWindowSize(winhandle, &xx, &yy);
		SDL_RestoreWindow(winhandle);
		SDL_GetWindowPosition(winhandle, &winpos_x, &winpos_y);
		SDL_GetWindowSize(winhandle, &winsize_w, &winsize_h);
		win_maximize = !(xx == winsize_w && yy == winsize_h);
	}
	SDL_GL_DeleteContext(conhandle);
	SDL_DestroyWindow(winhandle);
}

void GetInputString(const char *txt)
{
	std::cout << txt;
	std::getline(std::cin, inp_str);
}

void PrepareStringStream()
{
	ss.clear();
	ss.seekg(0);
	ss.str(inp_str);
}

void GetProjectName()
{
	std::cout << "Project name: ";
	std::getline(std::cin, prjfilename);
	prjimagename = prjfilename + ".tga";
	prjfilename = prjfilename + ".txt";
}

void NewProject()
{
	GetProjectName();

	int v_major, v_minor, v_glsl;
	bool flag;
	const char *parsepos;
  repeat_dim_req:
	img_w = img_h = 0;
	GetInputString("Image dimensions (e.g. `640x480`): ");
	parsepos = inp_str.c_str();
	while (1)
	{
		switch (*parsepos)
		{
		  case 'x':
			goto dobreak1;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			img_w = img_w * 10 + *parsepos - '0';
			break;
		  default:
			std::cout << baddatastr;
			goto repeat_dim_req;
			break;
		}
		goto skipbreak1;
	  dobreak1:
	  	parsepos++;
	  	break;
	  skipbreak1:
		parsepos++;
	}
	while (1)
	{
		switch (*parsepos)
		{
		  case '\0':
			goto dobreak2;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			img_h = img_h * 10 + *parsepos - '0';
			break;
		  default:
			std::cout << baddatastr;
			goto repeat_dim_req;
			break;
		}
		goto skipbreak2;
	  dobreak2:
	  	break;
	  skipbreak2:
		parsepos++;
	}
	if (img_w <= 0 || img_h <= 0)
	{
		std::cout << baddatastr;
		goto repeat_dim_req;
	}
	if (img_w > 0xffff) img_w = 0xffff;
	if (img_h > 0xffff) img_h = 0xffff;

  repeat_ver_req:
	v_major = v_minor = flag = 0;
	GetInputString("OpenGL context version (`3.0` or newer): ");
	parsepos = inp_str.c_str();
	while (1)
	{
		switch (*parsepos)
		{
		  case '.':
			goto dobreak3;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			v_major = v_major * 10 + *parsepos - '0';
			break;
		  default:
			std::cout << baddatastr;
			goto repeat_ver_req;
			break;
		}
		goto skipbreak3;
	  dobreak3:
	  	parsepos++;
	  	break;
	  skipbreak3:
		parsepos++;
	}
	while (1)
	{
		switch (*parsepos)
		{
		  case '\0':
			goto dobreak4;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			v_minor = v_minor * 10 + *parsepos - '0';
			break;
		  default:
			std::cout << baddatastr;
			goto repeat_ver_req;
			break;
		}
		goto skipbreak4;
	  dobreak4:
	  	break;
	  skipbreak4:
		parsepos++;

		flag = 1;
	}
	if (v_major < 3 || v_minor < 0 || !flag)
	{
		std::cout << baddatastr;
		goto repeat_ver_req;
	}

  repeat_glsl_ver_request:
	v_glsl = 0;
	GetInputString("GLSL version (`300` or newer): ");
	parsepos = inp_str.c_str();
	while (1)
	{
		switch (*parsepos)
		{
		  case '\0':
			goto dobreak5;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			v_glsl = v_glsl * 10 + *parsepos - '0';
			break;
		  default:
			std::cout << baddatastr;
			goto repeat_glsl_ver_request;
			break;
		}
		goto skipbreak5;
	  dobreak5:
	  	break;
	  skipbreak5:
		parsepos++;
	}
	if (v_glsl < 300 || parsepos == inp_str.c_str())
	{
		std::cout << baddatastr;
		goto repeat_glsl_ver_request;
	}

	bool compat_glsl = AskList("GLSL profile:", {{"core", "Core profile"}, {"cmp", "Compatibility profile"}});

	if (v_glsl >= 500 && !compat_glsl) std::cout << "Warning: Default vertex shader hasn't been tested on glsl 500+ core. If it does not work, try compatibility profile.\n";


	uni_tex_list.clear();
	if (!AskList("Do you need to load textures?", {{"y", "Yes"}, {"n", "No"}}))
	{
		std::string tmpstr;
		do
		{
		  repeat_uni_tex_name_req:
			GetInputString("Sampler uniform name: ");
			for (char it : inp_str)
			{
				if (!((it >= 'A' && it <= 'z') || (it >= '0' && it <= '9') || it == '_'))
				{
					std::cout << "Invalid character in string, only A-Z,a-z,0-9 are allowed.\n";
					goto repeat_uni_tex_name_req;
				}
			}
			if (inp_str[0] >= '0' && inp_str[0] <= '9')
			{
				std::cout << "0-9 can't be used as first character.\n";
				goto repeat_uni_tex_name_req;
			}
			if (inp_str[0] == '\0')
			{
				goto repeat_uni_tex_name_req;
			}
			for (const auto &it : uni_tex_list)
			{
				if (it.first == inp_str)
				{
					std::cout << "Duplicate uniform names are not allowed.\n";
					goto repeat_uni_tex_name_req;
				}
			}
			tmpstr = inp_str;
		  repeat_uni_tex_file_req:

			GetInputString("File name (without extension, `.tga` is always used): ");
			for (char it : inp_str)
			{
				if (it < 32 || it == '|')
				{
					std::cout << "Invalid character in string, \\x00-\\x1F and `|` are not allowed.\n";
					goto repeat_uni_tex_file_req;
				}
			}

			bool filt_linear = AskList("Interpolation mode:", {{"n", "Nearest neighbor"}, {"l", "Linear"}});
			int h_wrap = AskList("Horizontal wrap mode:", {{"c", "Clamp coordinates"}, {"m", "Mirror texture if out of range"}, {"r", "Repeat texture if out of range"}, {"t", "Transparent if out of range"}});
			int v_wrap = AskList("Vertical wrap mode:", {{"c", "Clamp coordinates"}, {"m", "Mirro texture if out of ranger"}, {"r", "Repeat texture if out of range"}, {"t", "Transparent if out of range"}});
			uni_tex_list.push_back(TextureData{tmpstr, inp_str, filt_linear, h_wrap, v_wrap, 0});
		}
		while (!AskList("Add another texture?", {{"y", "Yes"}, {"n", "No"}}));
	}

	const char table_interp[] = "NL", table_wrap[] = "_MRT";

	std::ofstream out_file(prjfilename.c_str());
	out_file << "/*" << v_major << '.' << v_minor << '@' << img_w << 'x' << img_h;
	for (auto &it : uni_tex_list)
		out_file << '|' << it.first << '|' << it.second << '|' << table_interp[it.filt_linear] << table_wrap[it.h_wrap] << table_wrap[it.v_wrap];
	out_file << "*/\n"
				""                                                                     "\n"
				"#version " << v_glsl; if (compat_glsl) out_file << " compatibility"; out_file << "\n";
	out_file << ""                                                                     "\n"
				"void main(){switch(gl_VertexID){case 0:gl_Position=vec4(1,1,0,1);break;"
				"case 1:gl_Position=vec4(-1,1,0,1);break;case 2:gl_Position=vec4(-1,-1,0,1);"
				"break;case 3:gl_Position=vec4(1,-1,0,1);break;default:gl_Position=vec4"
				"(0,0,0,1);}}"                                                         "\n"
                ""                                                                     "\n"
                "/*~~~~~~~*/"                                                          "\n"
                ""                                                                     "\n"
				"#version " << v_glsl << (compat_glsl ? " compatibility\n" : "\n");
	if (!uni_tex_list.empty())
	{
		out_file << '\n';
		for (auto &it : uni_tex_list)
			out_file << "uniform sampler2D " << it.first << ";\n";
	}
	out_file << ""                                                                     "\n"
				"layout(origin_upper_left, pixel_center_integer) in vec4 gl_FragCoord;""\n"
				"out vec4 out_c;"                                                      "\n"
				""                                                                     "\n"
				"void main()"                                                          "\n"
				"{"                                                                    "\n"
				"    out_c = vec4(1,gl_FragCoord.x/" << img_w << ".0f,gl_FragCoord.y/" << img_h << ".0f,1); // Your code goes here\n"
                "}"                                                                    "\n";

	std::cout << "Project `" << prjfilename << "` created.\nYou can edit it with any text editor.\n";
}

void OpenProject()
{
	std::ifstream inp_file(prjfilename.c_str());
	if (!inp_file)
	{
		std::cout << '`' << prjfilename << "` not found.\n";
		return;
	}
	bool need_preview = !AskList("Do you want preview window?", {{"y", "Yes"}, {"n", "No"}});
	if (need_preview)
	{
		zoom_level = 1;
		std::cout << "Controls:\n"
					 " Shift          - Reload project file\n"
					 " Alt            - Reload textures\n"
					 " Ctrl           - Save image\n"
					 " R Mouse Button - Increse zoom\n"
					 " L Mouse Button - Reset zoom\n"
					 " M Mouse Button - Hold to stretch image to window size\n"
					 " Esc            - Back to main menu\n"
					 " Close button   - Quit\n"
					 " Move mouse to see different parts of the image if it's bigger than the window\n\n";
	}
  reload:
	if (!OpenWindow(need_preview))
	{
		switch (AskList("Project:", {{"r", "Try to load project file again"}, {"m", "Back to main menu"}, {"q", "Quit"}}))
		{
		  case 0:
			goto reload;
			break;
		  case 1:
			return;
			break;
		  case 2:
			Exit();
			break;
		}
	}

	glViewport(0, 0, img_w, img_h);
	glDrawArrays(GL_QUADS, 0, 4);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	if (!need_preview)
	{
		std::cout << "File loaded.\n";
		SaveImage();
		switch (AskList("Project:", {{"r", "Reload project file and save image again"}, {"m", "Back to main menu"}, {"q", "Quit"}}))
		{
		  case 0:
			CloseWindow();
			goto reload;
			break;
		  case 1:
			CloseWindow();
			return;
			break;
		  case 2:
		  default:
			CloseWindow();
			Exit();
			break;
		}
	}
	std::cout << "File loaded.\n";

	float img_ratio = float(img_w) / float(img_h);
	uint32_t prev_stt = 0;
	while (1)
	{
		SDL_Event curevent;
		while (SDL_PollEvent(&curevent))
		{
			switch (curevent.type)
			{
			  case SDL_QUIT:
				Exit();
				break;
			  case SDL_KEYDOWN:
				if (!curevent.key.repeat)
				{
					switch (curevent.key.keysym.scancode)
					{
					  case SDL_SCANCODE_RSHIFT:
					  case SDL_SCANCODE_LSHIFT:
						CloseWindow();
						goto reload;
						break;
					  case SDL_SCANCODE_LALT:
					  case SDL_SCANCODE_RALT:
						LoadTextures();
						glDrawArrays(GL_QUADS, 0, 4);
						std::cout << "Textures loaded.\n";
						break;
					  case SDL_SCANCODE_LCTRL:
					  case SDL_SCANCODE_RCTRL:
						SaveImage();
						break;
					  case SDL_SCANCODE_ESCAPE:
						CloseWindow();
						return;
						break;
					  default:;
					}
					break;
				}
			}
		}
		if (glGetError())
		{
			CloseWindow();
			std::cout << "OpenGL error.\n";
			switch (AskList("Project:", {{"r", "Reload project file"}, {"m", "Back to main menu"}, {"q", "Quit"}}))
			{
			case 0:
				goto reload;
				break;
			case 1:
				return;
				break;
			case 2:
				Exit();
				break;
			}
			return;
		}

		int mx, my, real_win_w, real_win_h, win_w, win_h, sx1, sy1, sx2, sy2, dx1, dy1, dx2, dy2, i_w, i_h;
		int w_w, w_h;
		uint32_t stt = SDL_GetMouseState(&mx, &my);

		if ((stt & SDL_BUTTON(3)) && !(prev_stt & SDL_BUTTON(3)) && zoom_level < 8) zoom_level += 1;
		if ((stt & SDL_BUTTON(1)) && !(prev_stt & SDL_BUTTON(1))) zoom_level = 1;
		bool zoom_out = stt & SDL_BUTTON(2);

		if (zoom_out)
			SDL_SetWindowTitle(winhandle, "ShaderSandbox - Preview - STRETCHED");
		else if (zoom_level != 1)
			SDL_SetWindowTitle(winhandle, Str({"ShaderSandbox - Preview - ZOOM ", zoom_level, 'X'}));
		else
			SDL_SetWindowTitle(winhandle, "ShaderSandbox - Preview");

		prev_stt = stt;

		SDL_GetWindowSize(winhandle, &real_win_w, &real_win_h);
		SDL_GL_GetDrawableSize(winhandle, &win_w, &win_h);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT);

		i_w = img_w * zoom_level;
		i_h = img_h * zoom_level;
		w_w = double(win_w) / double(zoom_level);
		w_h = double(win_h) / double(zoom_level);
		if (!zoom_out)
		{
			if (i_w > win_w)
			{
				sx1 = (img_w - w_w) * (mx - real_win_w / 8.0) / (real_win_w * 0.75);
				sx2 = sx1 + w_w;
				dx1 = 0;
				dx2 = (sx2 - sx1) * zoom_level;

				if (sx1 < 0)
				{
					sx1 = 0;
					sx2 = w_w;
				}
				else if (sx2 >= img_w)
				{
					sx1 = img_w - w_w;
					sx2 = img_w;
				}
			}
			else
			{
				sx1 = 0;
				sx2 = img_w;
				dx1 = (win_w - i_w) / 2;
				dx2 = dx1 + i_w;
			}

			if (i_h > win_h)
			{
				sy1 = (img_h - w_h) * (1.0 - (my - real_win_h / 8.0) / (real_win_h * 0.75));
				sy2 = sy1 + w_h;
				dy1 = win_h - (sy2 - sy1) * zoom_level;
				dy2 = win_h;

				if (sy1 < 0)
				{
					sy1 = 0;
					sy2 = w_h;
				}
				else if (sy2 >= img_h)
				{
					sy1 = img_h - w_h;
					sy2 = img_h;
				}
			}
			else
			{
				sy1 = 0;
				sy2 = img_h;
				dy1 = (win_h - i_h) / 2;
				dy2 = dy1 + i_h;
			}
			glBlitFramebuffer(sx1, sy1, sx2, sy2, dx1, dy1, dx2, dy2, GL_COLOR_BUFFER_BIT, GL_NEAREST);
		}
		else
		{
			float win_ratio = float(win_w) / float(win_h);

			if (img_ratio > win_ratio)
			{
				int apos = std::round(win_h - win_w / img_ratio) / 2.0f;
				glBlitFramebuffer(0, 0, img_w, img_h, 0, apos, win_w, apos + win_w / img_ratio, GL_COLOR_BUFFER_BIT, GL_LINEAR);
			}
			else
			{
				int apos = std::round(win_w - win_h * img_ratio) / 2.0f;
				glBlitFramebuffer(0, 0, img_w, img_h, apos, 0, apos + win_h * img_ratio, win_h, GL_COLOR_BUFFER_BIT, GL_LINEAR);
			}
		}
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbuffer);
		SDL_GL_SwapWindow(winhandle);
	}
}

void ShowSpec()
{
	std::cout << "Project file spec:\n"
				 " `/*` GLMajor `.` GLMinor `@` ImageW `x` ImageH TextureList `*/`\n"
				 " VertexCode `/*~~~~~~~*/` FragmentCode\n"
				 "\n"
				 "TextureList - empty string or any number of textures' definitions without\n"
				 "              delimiters.\n"
				 "Texture definition:\n"
				 " `|` UniformName `|` FileName `|` InterpolationType\n"
				 " HorisontalWrapMode VerticalWrapMode\n"
				 "InterpolationType:\n"
				 " one of:\n"
				 "  `N`    // Nearest neighbor\n"
				 "  `L`    // Linear\n"
				 "HorisontalWrapMode, VerticalWrapMode:\n"
				 " one of:\n"
				 "  `_`    // Clamp coords\n"
				 "  `M`    // Repeat and mirror if out of range\n"
				 "  `R`    // Repeat if out of range\n"
				 "  `T`    // Transparent if out of range\n"
				 "\n"
				 "\n"
				 "Example:\n"
				 "/*4.3@800x600|uniform1|blahblah|N__|uniform2|blah|LMM*/\n"
				 "some code\n"
				 "/*~~~~~~~*/\n"
				 "even more code\n"
				 "\n";
}

void GenerateNoiseTex()
{
	GetInputString("File name (without extension, `.tga` is always used): ");
	std::string fname = inp_str + ".tga";

	const char *parsepos;
  repeat_dim_req:
	img_w = img_h = 0;
	GetInputString("Image dimensions (e.g. `640x480`): ");
	parsepos = inp_str.c_str();
	while (1)
	{
		switch (*parsepos)
		{
		  case 'x':
			goto dobreak1;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			img_w = img_w * 10 + *parsepos - '0';
			break;
		  default:
			std::cout << baddatastr;
			goto repeat_dim_req;
		}
		goto skipbreak1;
	  dobreak1:
	  	parsepos++;
	  	break;
	  skipbreak1:
		parsepos++;
	}
	while (1)
	{
		switch (*parsepos)
		{
		  case '\0':
			goto dobreak2;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
			img_h = img_h * 10 + *parsepos - '0';
			break;
		  default:
			std::cout << baddatastr;
			goto repeat_dim_req;
		}
		goto skipbreak2;
	  dobreak2:
	  	break;
	  skipbreak2:
		parsepos++;
	}
	if (img_w <= 0 || img_h <= 0)
	{
		std::cout << baddatastr;
		goto repeat_dim_req;
	}
	if (img_w > 0xffff) img_w = 0xffff;
	if (img_h > 0xffff) img_h = 0xffff;

	int seed;
	bool flag = 0;
  repeat_seed_req:
  	seed = 0;
  	GetInputString("Seed (non-negative number): ");
	parsepos = inp_str.c_str();
	while (1)
	{
		switch (*parsepos)
		{
		  case '\0':
			goto dobreak3;
		  case '0': case '1': case '2': case '3': case '4':
		  case '5': case '6': case '7': case '8': case '9':
		  	seed = seed * 10 + *parsepos - '0';
		  	flag = 1;
		  	break;
		  default:
			std::cout << baddatastr;
			goto repeat_seed_req;
		}

		parsepos++;

		continue;
	  dobreak3:
		break;
	}
	if (!flag)
	{
		std::cout << baddatastr;
		goto repeat_seed_req;
	}

	std::ofstream out_f(fname.c_str(), std::ios_base::out | std::ios_base::binary);
	if (!out_f)
	{
		std::cout << "Can't save image to `" << fname << "`.\n";
		return;
	}
	out_f << (unsigned char)(0x00); // 00
	out_f << (unsigned char)(0x00); // 01
	out_f << (unsigned char)(0x02); // 02
	out_f << (unsigned char)(0x00); // 03
	out_f << (unsigned char)(0x00); // 04
	out_f << (unsigned char)(0x00); // 05
	out_f << (unsigned char)(0x00); // 06
	out_f << (unsigned char)(0x00); // 07
	out_f << (unsigned char)(0x00); // 08
	out_f << (unsigned char)(0x00); // 09
	out_f << (unsigned char)(0x00); // 0A
	out_f << (unsigned char)(0x00); // 0B
	out_f << (unsigned char)(img_w % 256); // 0C
	out_f << (unsigned char)(img_w / 256); // 0D
	out_f << (unsigned char)(img_h % 256); // 0E
	out_f << (unsigned char)(img_h / 256); // 0F
	out_f << (unsigned char)(0x20); // 10
	out_f << (unsigned char)(0x08); // 11

	static constexpr int bufsize = 1024;

	uint32_t *arr = new uint32_t[bufsize];

	int len = img_w * img_h;

	std::mt19937 rng;
	rng.seed(seed);
	rng.discard(1024);

	while (len)
	{
		if (len >= bufsize)
		{
			for (int i = 0; i < bufsize; i++)
				arr[i] = rng();
			out_f.write((char *) arr, bufsize * 4);
			len -= bufsize;
		}
		else
		{
			for (int i = 0; i < len; i++)
				arr[i] = rng();
			out_f.write((char *) arr, len * 4);
			break;
		}
	}

	delete [] arr;

	std::cout << "Image saved.\n";
}

int main(int argc, char **argv)
{
	std::cout << "ShaderSandbox  v1.8                               \n"
				 "                                                  \n"
				 "    by HolyBlackCat (std-cin@mail.ru), Mar 27 2015\n"
				 "                                                  \n";

	if (argc > 2)
		std::cout << "Too many command line arguments.\n";
	else if (argc == 2)
	{
		prjimagename = prjfilename = argv[1];
		auto begin = prjimagename.begin(), end = --prjimagename.end();
		if (begin == end) goto stop_replace;
		if (*end == '"')
			end--;
		if (begin == end) goto stop_replace;
		if (*end == 't')
		{
			*end = 'a';
			end--;
		}
		if (begin == end) goto stop_replace;
		if (*end == 'x')
		{
			*end = 'g';
			end--;
		}
		if (begin == end) goto stop_replace;
		if (*end == 't')
		{
			*end = 't';
			end--;
		}

		goto direct_open;
	  stop_replace: ;
	}

	Init();

	while (1)
	{
		switch (AskList("Main menu:", {{"n", "New project"}, {"o", "Open project"}, {"g", "Generate noise texture"}, {"s", "Show project file specification"}, {"l", "Show update and source links"}, {"q", "Quit"}}))
		{
		  case 0:
			NewProject();
			break;
		  case 1:
		  	GetProjectName();
		  direct_open:
			OpenProject();
			break;
		  case 2:
			GenerateNoiseTex();
			break;
		  case 3:
		  	ShowSpec();
			break;
		  case 4:
		  	switch (AskList("What link do you need? (It will be copied to clipboard.)", {{"1", "Latest binary"}, {"2", "Source code"}, {"m", "Back to main menu"}}))
		  	{
			  case 0:
				SDL_SetClipboardText("https://bitbucket.org/HolyBlackCat/ld48/downloads/ShaderSandbox.exe");
				break;
			  case 1:
				SDL_SetClipboardText("https://bitbucket.org/HolyBlackCat/ld48/src");
				break;
		  	}
			break;
		  case 5:
			Exit();
			break;
		}
	}

	Exit();
	return 0;
}